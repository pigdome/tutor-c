#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

const double USD = 32.86;
const double JPY = 0.29;

double thb_to_usd(double thb)
{
	return thb / USD;	
}

double thb_to_jpy(double thb)
{
	return thb / JPY;	
}

int main()
{
	double thb = 5000;
	double usd = thb_to_usd(thb);
	double jpy = thb_to_jpy(thb);

	printf("My USD is %f\n", usd);
	printf("My JPY is %f\n", jpy);
}
