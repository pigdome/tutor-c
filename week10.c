#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void check( int a, float b, double c )
{
	printf("The integer is %d\n", a);
	printf("The floating point number is %f\n", b);
	printf("The double precision number is %f\n", c);
}

void find_abs(double number)
{
	printf("The absolute value of %f is %f\n", number, number * (-1) );
}

void hilo()
{
	srand(time(NULL));
	int number = 1 + rand() % 100;
	int lose = 1;

	for( int i = 0; i < 7; i++ )
	{
		int guess;
		printf("Enter your guess:\n");
		scanf("%d", &guess);
		
		if( number == guess )
		{
			printf("Hooray, you have won!\n");
			--lose;
			break;
		}
		else if( number > guess )
		{
			printf("Wrong number :( Your guess was too low.\n");
		}
		else
		{
			printf("Wrong number :( Your guess was too high.\n");
		}
	}
	
	if( lose )
	{
		printf("Sorry, you lose and the correct number is %d\n", number);
	}
}

double euc_dist(double x1, double y1, double x2, double y2)
{
	return sqrt( pow( (x2 - x1), 2 ) + pow( (y2 - y1), 2 ) );
}

void main()
{
	// LAP 1:
	/*
	int a;
	float b;
	double c;

	printf("Enter an integer: \n");
	scanf("%d", &a);

	printf("Enter a floating point number: \n");
	scanf("%f", &b);
	
	printf("Enter a double precision number: \n");
	scanf("%lf", &c);

	check(a, b, c);
	*/
	// ---------------------------------------------------
	// LAP 2:
	/*
	double number;

	printf("Enter a number:\n");
	scanf("%lf", &number);

	find_abs(number);
	*/
	// ---------------------------------------------------
	// LAP 3:
	//hilo();	
	// ---------------------------------------------------
	// BONUS :
	double x1;
	double y1;
	double x2;
	double y2;

	printf("Please enter a value for x1\n");
	scanf("%lf", &x1);
	
	printf("Please enter a value for y1\n");
	scanf("%lf", &y1);
	
	printf("Please enter a value for x2\n");
	scanf("%lf", &x2);
	
	printf("Please enter a value for y2\n");
	scanf("%lf", &y2);

	double distance = euc_dist(x1, y1, x2, y2);
	printf("The distance between the point is %f\n", distance);
}	
