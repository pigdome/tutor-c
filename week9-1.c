#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	srand(time(NULL));
	
	int row = 3;
	int col = 4;
	int metric[row][col];

	// assign value into metric
	for( int i = 0; i < row; i++ )
	{
		for( int j = 0; j < col; j++ )
		{
			metric[i][j] = rand() % 10;
		}
	}
	
	// display metric
	for( int i = 0; i < row; i++ )
	{
		for( int j = 0; j < col; j++ )
		{
			int value = metric[i][j];
			printf("%d ", value);
		}
		printf("\n");
	}
	
	// find sum of rows
	printf("Sum of rows: ");
	for( int i = 0; i < row; i++ )
	{
		int sum = 0;
		for( int j = 0; j < col; j++ )
		{
			sum += metric[i][j];
		}
		printf("%d ", sum);
	}
	printf("\n");

	// find sum of columns
	printf("Sum of columns: ");
	for( int j = 0; j < col; j++ )
	{
		int sum = 0;
		for( int i = 0; i < row; i++ )
		{
			sum += metric[i][j];
		}
		printf("%d ", sum);
	}
	printf("\n");

	return 0;
} 
